FROM nginx:1.15.6-alpine

COPY nginx.conf /etc/nginx/nginx.conf
RUN mkdir -p /var/www/usctr.co /cache
RUN chown nginx:nginx /cache
COPY html /var/www/usctr.co
